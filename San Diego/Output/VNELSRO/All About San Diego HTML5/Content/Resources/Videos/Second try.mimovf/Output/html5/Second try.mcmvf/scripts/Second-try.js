var stage;

var Movies = [
	{id: "e4ADD4dvV0W2URgurZctIA", url: "Second try.htm"},
];

var MovieIndex = 0;
var MovieId = 'e4ADD4dvV0W2URgurZctIA';

var MovieProps = {
	frame1 : { id: 'NAAJlZr6j0ePNZWX_V9m_g', pause: false, frameStart: "0", frameDuration : "2192", transitionEffect : "", transitionDirection : "", transitionDuration : "", hasVideo : false },
	frame2 : { id: 'xDnFsb7Xe0iU-spc4jtoAA', pause: false, frameStart: "2192", frameDuration : "1550", transitionEffect : "", transitionDirection : "", transitionDuration : "", hasVideo : true },
	frame3 : { id: 'c9DnyAoetkKxcAnccSxloA', pause: false, frameStart: "3742", frameDuration : "1512", transitionEffect : "", transitionDirection : "", transitionDuration : "", hasVideo : false },
	frame4 : { id: 'yuHgD1o17E6Jn2rN_-Z4ew', pause: false, frameStart: "5254", frameDuration : "4176", transitionEffect : "", transitionDirection : "", transitionDuration : "", hasVideo : false },
	frame5 : { id: '9e9n8POOVUq6NjBEuUzAHw', pause: false, frameStart: "9430", frameDuration : "2881", transitionEffect : "", transitionDirection : "", transitionDuration : "", hasVideo : false },
	frame6 : { id: 'xQ6g0Tudg0-wUdsDkSFIUw', pause: false, frameStart: "12311", frameDuration : "1861", transitionEffect : "", transitionDirection : "", transitionDuration : "", hasVideo : false },
	frame7 : { id: 'qR9f75Bed0eJMXXcJEygKw', pause: false, frameStart: "14172", frameDuration : "3145", transitionEffect : "", transitionDirection : "", transitionDuration : "", hasVideo : false },
	frame8 : { id: 'oLKj2jxcm02QzNP_Z49lJw', pause: false, frameStart: "17317", frameDuration : "6535", transitionEffect : "", transitionDirection : "", transitionDuration : "", hasVideo : false }
};

var MovieShapes = {
	frame1: [
		{
			shape: "background",
			isActionShape: false,
			x: 0,
			y: 0,
			width: 274,
			height: 296,
			rotation: 0,
			offset: { x: 137, y: 148 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 2192,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#a9a9a9", endColor: "#a9a9a9" },
			imageSource: "images/Second-try/frame0.png",
		},
		{
			shape: "rect",
			id: "F20uf61pl02-OilhXPauLg",
			isActionShape: false,
			x: 106,
			y: 12,
			width: 48,
			height: 48,
			rotation: 0,
			offset: { x: 24, y: 24 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 0,
			shadow: {  },
			start: 0,
			duration: 2192,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "transparent", endColor: "transparent" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 0, right: 0, bottom: 0, left: 0 }  },
			visualStates: {
			},
			clickAction : [{ action: "GotoNext", args: {} }, ],
			masks: [
			],
		},
		{
			shape: "cursor-ellipse",
			id: "bwg7h_eiOUaHr4ggil8RrQ",
			isActionShape: false,
			x: 105,
			y: 14,
			width: 34,
			height: 34,
			rotation: 0,
			offset: { x: 17, y: 17 },
			opacity: 0.2,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 2192,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#ffa500", endColor: "#ffa500" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 5, right: 5, bottom: 5, left: 5 }  },
			visualStates: {
			},
			imageSource: "images/Second-try/arrow-cursor.png",
			imageWidth: 32,
			imageHeight: 32,
			cursorHotspot: { x: 0, y: 0 },
			positionKeyFrames: [ 
				{ targetTime: 0, targetValue: {x: 105, y: 14}, interpolation: "linear", inValue: {x: 85, y: -6}, outValue: {x: 125, y: 34} }, 
				{ targetTime: 1892, targetValue: {x: 105, y: 11}, interpolation: "linear", inValue: {x: 85, y: -9}, outValue: {x: 125, y: 31} }, 
			],
			opacityKeyFrames: [ 
				{ targetTime: 1892, targetValue: 0.2, interpolation: "linear" }, 
				{ targetTime: 1992, targetValue: 0.6, interpolation: "linear" }, 
				{ targetTime: 2092, targetValue: 0.2, interpolation: "linear" }, 
			],
			masks: [
			],
		},
	],
	frame2: [
	],
	frame3: [
		{
			shape: "background",
			isActionShape: false,
			x: 0,
			y: 0,
			width: 274,
			height: 296,
			rotation: 0,
			offset: { x: 137, y: 148 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 1512,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#a9a9a9", endColor: "#a9a9a9" },
			imageSource: "images/Second-try/frame2.png",
		},
		{
			shape: "rect",
			id: "XcQjd9jUL0uuLy-Uoq2hzQ",
			isActionShape: false,
			x: 106,
			y: 12,
			width: 48,
			height: 48,
			rotation: 0,
			offset: { x: 24, y: 24 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 0,
			shadow: {  },
			start: 0,
			duration: 1512,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "transparent", endColor: "transparent" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 0, right: 0, bottom: 0, left: 0 }  },
			visualStates: {
			},
			clickAction : [{ action: "GotoNext", args: {} }, ],
			masks: [
			],
		},
		{
			shape: "cursor-ellipse",
			id: "bwg7h_eiOUaHr4ggil8RrQ",
			isActionShape: false,
			x: 105,
			y: 11,
			width: 34,
			height: 34,
			rotation: 0,
			offset: { x: 17, y: 17 },
			opacity: 0.2,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 1512,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#ffa500", endColor: "#ffa500" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 5, right: 5, bottom: 5, left: 5 }  },
			visualStates: {
			},
			imageSource: "images/Second-try/arrow-cursor.png",
			imageWidth: 32,
			imageHeight: 32,
			cursorHotspot: { x: 0, y: 0 },
			positionKeyFrames: [ 
				{ targetTime: 0, targetValue: {x: 105, y: 11}, interpolation: "linear", inValue: {x: 85, y: -9}, outValue: {x: 125, y: 31} }, 
				{ targetTime: 1212, targetValue: {x: 105, y: 11}, interpolation: "linear", inValue: {x: 85, y: -9}, outValue: {x: 125, y: 31} }, 
			],
			opacityKeyFrames: [ 
				{ targetTime: 1212, targetValue: 0.2, interpolation: "linear" }, 
				{ targetTime: 1312, targetValue: 0.6, interpolation: "linear" }, 
				{ targetTime: 1412, targetValue: 0.2, interpolation: "linear" }, 
			],
			masks: [
			],
		},
	],
	frame4: [
		{
			shape: "background",
			isActionShape: false,
			x: 0,
			y: 0,
			width: 274,
			height: 296,
			rotation: 0,
			offset: { x: 137, y: 148 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 4176,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#a9a9a9", endColor: "#a9a9a9" },
			imageSource: "images/Second-try/frame3.png",
		},
		{
			shape: "rect",
			id: "7JDQaqOe3U6xCfZSi3rC_A",
			isActionShape: false,
			x: 151,
			y: 34,
			width: 48,
			height: 48,
			rotation: 0,
			offset: { x: 24, y: 24 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 0,
			shadow: {  },
			start: 0,
			duration: 4176,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "transparent", endColor: "transparent" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 0, right: 0, bottom: 0, left: 0 }  },
			visualStates: {
			},
			clickAction : [{ action: "GotoNext", args: {} }, ],
			masks: [
			],
		},
		{
			shape: "cursor-ellipse",
			id: "bwg7h_eiOUaHr4ggil8RrQ",
			isActionShape: false,
			x: 109,
			y: 11,
			width: 34,
			height: 34,
			rotation: 0,
			offset: { x: 17, y: 17 },
			opacity: 0.2,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 4176,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#ffa500", endColor: "#ffa500" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 5, right: 5, bottom: 5, left: 5 }  },
			visualStates: {
			},
			imageSource: "images/Second-try/arrow-cursor.png",
			imageWidth: 32,
			imageHeight: 32,
			cursorHotspot: { x: 0, y: 0 },
			positionKeyFrames: [ 
				{ targetTime: 0, targetValue: {x: 109, y: 11}, interpolation: "linear", inValue: {x: 89, y: -9}, outValue: {x: 129, y: 31} }, 
				{ targetTime: 3876, targetValue: {x: 150, y: 33}, interpolation: "linear", inValue: {x: 130, y: 13}, outValue: {x: 170, y: 53} }, 
			],
			opacityKeyFrames: [ 
				{ targetTime: 3876, targetValue: 0.2, interpolation: "linear" }, 
				{ targetTime: 3976, targetValue: 0.6, interpolation: "linear" }, 
				{ targetTime: 4076, targetValue: 0.2, interpolation: "linear" }, 
			],
			masks: [
			],
		},
	],
	frame5: [
		{
			shape: "background",
			isActionShape: false,
			x: 0,
			y: 0,
			width: 274,
			height: 296,
			rotation: 0,
			offset: { x: 137, y: 148 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 2881,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#a9a9a9", endColor: "#a9a9a9" },
			imageSource: "images/Second-try/frame4.png",
		},
		{
			shape: "rect",
			id: "PgrSLswz30iCa6D0uxhc6Q",
			isActionShape: false,
			x: 217,
			y: 40,
			width: 48,
			height: 48,
			rotation: 0,
			offset: { x: 24, y: 24 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 0,
			shadow: {  },
			start: 0,
			duration: 2881,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "transparent", endColor: "transparent" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 0, right: 0, bottom: 0, left: 0 }  },
			visualStates: {
			},
			clickAction : [{ action: "GotoNext", args: {} }, ],
			masks: [
			],
		},
		{
			shape: "cursor-ellipse",
			id: "bwg7h_eiOUaHr4ggil8RrQ",
			isActionShape: false,
			x: 153,
			y: 33,
			width: 34,
			height: 34,
			rotation: 0,
			offset: { x: 17, y: 17 },
			opacity: 0.2,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 2881,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#ffa500", endColor: "#ffa500" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 5, right: 5, bottom: 5, left: 5 }  },
			visualStates: {
			},
			imageSource: "images/Second-try/arrow-cursor.png",
			imageWidth: 32,
			imageHeight: 32,
			cursorHotspot: { x: 0, y: 0 },
			positionKeyFrames: [ 
				{ targetTime: 0, targetValue: {x: 153, y: 33}, interpolation: "linear", inValue: {x: 133, y: 13}, outValue: {x: 173, y: 53} }, 
				{ targetTime: 2581, targetValue: {x: 216, y: 39}, interpolation: "linear", inValue: {x: 196, y: 19}, outValue: {x: 236, y: 59} }, 
			],
			opacityKeyFrames: [ 
				{ targetTime: 2581, targetValue: 0.2, interpolation: "linear" }, 
				{ targetTime: 2681, targetValue: 0.6, interpolation: "linear" }, 
				{ targetTime: 2781, targetValue: 0.2, interpolation: "linear" }, 
			],
			masks: [
			],
		},
	],
	frame6: [
		{
			shape: "background",
			isActionShape: false,
			x: 0,
			y: 0,
			width: 274,
			height: 296,
			rotation: 0,
			offset: { x: 137, y: 148 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 1861,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#a9a9a9", endColor: "#a9a9a9" },
			imageSource: "images/Second-try/frame5.png",
		},
		{
			shape: "rect",
			id: "vloNV-aEQ0eIv8nVaKC-tg",
			isActionShape: false,
			x: 197,
			y: 99,
			width: 48,
			height: 48,
			rotation: 0,
			offset: { x: 24, y: 24 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 0,
			shadow: {  },
			start: 0,
			duration: 1861,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "transparent", endColor: "transparent" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 0, right: 0, bottom: 0, left: 0 }  },
			visualStates: {
			},
			clickAction : [{ action: "GotoNext", args: {} }, ],
			masks: [
			],
		},
		{
			shape: "cursor-ellipse",
			id: "bwg7h_eiOUaHr4ggil8RrQ",
			isActionShape: false,
			x: 215,
			y: 43,
			width: 34,
			height: 34,
			rotation: 0,
			offset: { x: 17, y: 17 },
			opacity: 0.2,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 1861,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#ffa500", endColor: "#ffa500" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 5, right: 5, bottom: 5, left: 5 }  },
			visualStates: {
			},
			imageSource: "images/Second-try/arrow-cursor.png",
			imageWidth: 32,
			imageHeight: 32,
			cursorHotspot: { x: 0, y: 0 },
			positionKeyFrames: [ 
				{ targetTime: 0, targetValue: {x: 215, y: 43}, interpolation: "linear", inValue: {x: 195, y: 23}, outValue: {x: 235, y: 63} }, 
				{ targetTime: 1561, targetValue: {x: 196, y: 98}, interpolation: "linear", inValue: {x: 176, y: 78}, outValue: {x: 216, y: 118} }, 
			],
			opacityKeyFrames: [ 
				{ targetTime: 1561, targetValue: 0.2, interpolation: "linear" }, 
				{ targetTime: 1661, targetValue: 0.6, interpolation: "linear" }, 
				{ targetTime: 1761, targetValue: 0.2, interpolation: "linear" }, 
			],
			masks: [
			],
		},
	],
	frame7: [
		{
			shape: "background",
			isActionShape: false,
			x: 0,
			y: 0,
			width: 274,
			height: 296,
			rotation: 0,
			offset: { x: 137, y: 148 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 3145,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#a9a9a9", endColor: "#a9a9a9" },
			imageSource: "images/Second-try/frame6.png",
		},
		{
			shape: "rect",
			id: "Cj8dbqqFtUaHvQezUE4cOw",
			isActionShape: false,
			x: 202,
			y: 240,
			width: 48,
			height: 48,
			rotation: 0,
			offset: { x: 24, y: 24 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 0,
			shadow: {  },
			start: 0,
			duration: 3145,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "transparent", endColor: "transparent" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 0, right: 0, bottom: 0, left: 0 }  },
			visualStates: {
			},
			clickAction : [{ action: "GotoNext", args: {} }, ],
			masks: [
			],
		},
		{
			shape: "cursor-ellipse",
			id: "bwg7h_eiOUaHr4ggil8RrQ",
			isActionShape: false,
			x: 196,
			y: 98,
			width: 34,
			height: 34,
			rotation: 0,
			offset: { x: 17, y: 17 },
			opacity: 0.2,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 3145,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#ffa500", endColor: "#ffa500" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 5, right: 5, bottom: 5, left: 5 }  },
			visualStates: {
			},
			imageSource: "images/Second-try/arrow-cursor.png",
			imageWidth: 32,
			imageHeight: 32,
			cursorHotspot: { x: 0, y: 0 },
			positionKeyFrames: [ 
				{ targetTime: 0, targetValue: {x: 196, y: 98}, interpolation: "linear", inValue: {x: 176, y: 78}, outValue: {x: 216, y: 118} }, 
				{ targetTime: 2845, targetValue: {x: 201, y: 239}, interpolation: "linear", inValue: {x: 181, y: 219}, outValue: {x: 221, y: 259} }, 
			],
			opacityKeyFrames: [ 
				{ targetTime: 2845, targetValue: 0.2, interpolation: "linear" }, 
				{ targetTime: 2945, targetValue: 0.6, interpolation: "linear" }, 
				{ targetTime: 3045, targetValue: 0.2, interpolation: "linear" }, 
			],
			masks: [
			],
		},
	],
	frame8: [
		{
			shape: "background",
			isActionShape: false,
			x: 0,
			y: 0,
			width: 274,
			height: 296,
			rotation: 0,
			offset: { x: 137, y: 148 },
			opacity: 1,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 6535,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#a9a9a9", endColor: "#a9a9a9" },
			imageSource: "images/Second-try/frame7.png",
		},
		{
			shape: "cursor-ellipse",
			id: "bwg7h_eiOUaHr4ggil8RrQ",
			isActionShape: false,
			x: 204,
			y: 246,
			width: 34,
			height: 34,
			rotation: 0,
			offset: { x: 17, y: 17 },
			opacity: 0.2,
			stroke: "#000000",
			strokeWidth: 1,
			shadow: {  },
			start: 0,
			duration: 6535,
			cornerRadius: 0,
			fill: { type: "solid", startColor: "#ffa500", endColor: "#ffa500" },
			text: { x: 0, y: 0, width: 1, height: 1, align: "center", verticalAlign: "middle", padding: { top: 5, right: 5, bottom: 5, left: 5 }  },
			visualStates: {
			},
			imageSource: "images/Second-try/arrow-cursor.png",
			imageWidth: 32,
			imageHeight: 32,
			cursorHotspot: { x: 0, y: 0 },
			masks: [
			],
		},
	],
};



var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

    
var VerifyBrowserCompatibility = function(generateWebM, generateH264){
    $.browser.chrome = /chrome/.test(navigator.userAgent.toLowerCase());
    var ua = $.browser;
    var compatible;

    if (navigator.userAgent.match(/iPhone|iPod/i)){
        $("#movie_media").empty();
        $("#movie_media").attr("src", "videos/Second-try/Second try.mp4");
        return false;
    }

    if (generateWebM && generateH264){
        compatible = true;
    } else if (generateWebM) {
        compatible = Modernizr.video.webm || (Modernizr.video.webm == "probably") ;
        if (!compatible){
            $("div.incompatibleWrapper").css("visibility", "visible");
            $("div.incompatibleOverlay").html("This browser does not support the WebM output format. Please try the latest version of one of the following browsers:<br/><br/>Google Chrome<br/>Mozilla Firefox<br/>Opera");
        }
        return compatible;
    } else if (generateH264){
        compatible = Modernizr.video.h264 || (Modernizr.video.h264 == "probably");
        if (!compatible){
            $("div.incompatibleWrapper").css("visibility", "visible");
            $("div.incompatibleOverlay").html("This browser does not support the H.264 output format. Please try the latest version of one of the following browsers:<br/><br/>Google Chrome<br/>Microsoft Internet Explorer 9 or later<br/>Apple Safari (requires Quicktime on Windows)");
        }
    }
    
    return compatible;
}
 
           
var LoadAndStartMovie = function(){
    Movie.DownloadMedia(function () {
        if (Movie.isRecording()) {
            Movie.Initialize();
        }
        else {
            if (isMobile.any()){
                $("div.playOverlayWrapper").css("visibility", "visible");
                $("a.playButton").removeClass("playing").addClass("paused");
                $(".volumeWrapper").css("display", "none");
            } else {
                StartMovie();
            }
        }
    });
};


var StartMobile = function(){
    $("div.playOverlayWrapper").css("visibility", "hidden");
    
    StartMovie();
};


var StartMovie = function(){
    Movie.Initialize();
    if (Movie.video != null && !isMobile.any()) {
        Movie.video.addEventListener("canplay", OnCanPlayThrough, false);
        Movie.video.addEventListener("load", OnCanPlayThrough, false);
        Movie.video.addEventListener("error", OnError, false);
        if (Movie.video.readyState == 4){
            OnCanPlayThrough();
        }
        else {
            Movie.Play();
        }
    } else {
        Movie.Play();
    }
};

var OnCanPlayThrough = function(){
    Movie.video.removeEventListener("canplay", OnCanPlayThrough, false);
    Movie.video.removeEventListener("load", OnCanPlayThrough, false);
    Movie.Play();
}

var OnError = function(error){
    alert(error);
}


$(document).ready(function(){
    if ((typeof Movie !== 'undefined' && Movie.isRecording()) || VerifyBrowserCompatibility(true, false)){
        stage = new Konva.Stage({
            container: "KonvaHost",
            width: 274,
            height: 296
        });

        stage.getContent().style.position = "absolute";

        Movie.lastFrameIndex = 8;
        LoadAndStartMovie();
    }
});


